#!/bin/python3

import sys
 
def get_key(val, my_dict): 
    for key, value in my_dict.items(): 
         if val == value: 
             return key 
  
    return "Unknown state."

def states_expert(capital_city):
    states = {
        "Oregon"    : "OR",
        "Alabama"   : "AL",
        "New Jersey": "NJ",
        "Colorado"  : "CO"
    }

    capital_cities = {
        "OR": "Salem",
        "AL": "Montgomery",
        "NJ": "Trenton",
        "CO": "Denver"
    }

    city_sh = get_key(capital_city, capital_cities)
    result = get_key(city_sh, states)
    print(result)

if __name__ == '__main__':
    if len(sys.argv) == 2:
        states_expert(sys.argv[1])
    else:
        sys.exit()
