#!/bin/python3

# print (type(d)) ---> <class 'list'>
# print (type(d[1])) ---> <class 'tuple'>
# so, this is a list of tuples!

def convert(list_of_tuples, new_dict):
    # new_dict = dict((y, x) for x, y in list_of_tups)
    for key, value in list_of_tuples:
        if value in new_dict:
            new_dict[key].append(value)
        else:
            new_dict[key] = value

    return (new_dict)

def print_dict(newdict):
    for x, y in newdict.items():
        print(y, ':', x)

def dictionary_handler():
    d = [
        ('Hendrix'   , '1942'),
        ('Allman'    , '1946'),
        ('King'      , '1925'),
        ('Clapton'   , '1945'),
        ('Johnson'   , '1911'),
        ('Berry'     , '1926'),
        ('Vaughan'   , '1954'),
        ('Cooder'    , '1947'),
        ('Page'      , '1944'),
        ('Richards'  , '1943'),
        ('Hammett'   , '1962'),
        ('Cobain'    , '1967'),
        ('Garcia'    , '1942'),
        ('Beck'      , '1944'),
        ('Santana'   , '1947'),
        ('Ramone'    , '1948'),
        ('White'     , '1975'),
        ('Frusciante', '1970'),
        ('Thompson'  , '1949'),
        ('Burton'    , '1939')
]
    newdict = {}
    print_dict(convert(d, newdict))

if __name__ == '__main__':
    dictionary_handler()