#!/bin/python3

import sys

def my_var():
    list_of_everything = [42, '42', 'quarante-deux', 42.0, True, [42], {42: 42}, (42,), set()]
    for x in range (len(list_of_everything)):
        sys.stdout.write(str(list_of_everything[x]) + ' est de type ')
        print (type((list_of_everything[x])))

if __name__ == '__main__':
    my_var()
