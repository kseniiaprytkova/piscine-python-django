#!/bin/python3

import sys

def states_expert(state_full):
    states = {
        "Oregon"    : "OR",
        "Alabama"   : "AL",
        "New Jersey": "NJ",
        "Colorado"  : "CO"
    }

    capital_cities = {
        "OR": "Salem",
        "AL": "Montgomery",
        "NJ": "Trenton",
        "CO": "Denver"
    }

    state_sh = states.get(state_full, "Unknown state.")
    result = capital_cities.get(state_sh, "Unknown state.")
    print(result)

if __name__ == '__main__':
    if len(sys.argv) == 2:
        states_expert(sys.argv[1])
    else:
    	sys.exit()
