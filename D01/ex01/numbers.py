#!/bin/python3

import sys

def i_will_read_file():
    f = open("numbers.txt", "r")
    string = f.readline()
    array = string.split(",")
    for i, word in enumerate(array):
        sys.stdout.write(word)
        if i != (len(array) - 1):
            print()
    f.close()

if __name__ == '__main__':
    i_will_read_file()
