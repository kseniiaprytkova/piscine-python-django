#!/bin/python3

import sys

def get_key(val, my_dict): 
    for key, value in my_dict.items(): 
         if val == value: 
             return key 
  
    return(None)

def capital_expert(states, capital_cities, state_full):
    state_sh = states.get(state_full, None)
    result = capital_cities.get(state_sh, None)
    return(result)

def states_expert(states, capital_cities, capital_city):
    city_sh = get_key(capital_city, capital_cities)
    result = get_key(city_sh, states)
    return(result)

if __name__ == '__main__':
    if len(sys.argv) == 2:
        states = {
        "Oregon"    : "OR",
        "Alabama"   : "AL",
        "New Jersey": "NJ",
        "Colorado"  : "CO"
        }

        capital_cities = {
        "OR": "Salem",
        "AL": "Montgomery",
        "NJ": "Trenton",
        "CO": "Denver"
        }
        arr = sys.argv[1].split(",")
        for i in range(len(arr)):
            arr_withot_sp = arr[i].strip()
            res_arr = arr_withot_sp.title()
            result = capital_expert(states, capital_cities, res_arr)
            if result == None:
                    result = states_expert(states, capital_cities, res_arr)
                    if result == None:
                        if (any(c.isalpha() for c in arr_withot_sp)) == False:
                            continue
                        print(arr_withot_sp + " is neither a capital city nor a state")
                    else:
                        print(res_arr + " is the capital of " + result)
            else:
                print(result + " is the capital of " + res_arr)
    else:
    	sys.exit()
