#!/bin/python3

from local_lib.path import Path

if __name__ == '__main__':
    d = Path('/tmp/hello_path')
    d.mkdir_p()
    f = Path('/tmp/hello_path/test.txt')
    for f in d.files('*.txt'):
        f.chmod(0o755)
    f.touch()
    f.open()
    f.write_text("Space Goat")
    print(f.text())
