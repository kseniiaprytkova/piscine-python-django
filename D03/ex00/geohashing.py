#!/bin/python3

import sys
import antigravity

# python3 geohashing.py 2005-05-26 10458.68 37.421542 -122.085589
def geohashing_algo(date, dow, latitude, longitude):
    datedow = date + '-' + dow
    antigravity.geohash(float(latitude), float(longitude), datedow.encode('ascii'))

if __name__ == '__main__':
    if len(sys.argv) != 5:
        print("Error: i need 4 parameters:\n[date[ex:2005-05-26] DOW[ex:10458.68] latitude[ex:37.421542] longitude[ex:-122.085589]]")
        exit()
    geohashing_algo(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
