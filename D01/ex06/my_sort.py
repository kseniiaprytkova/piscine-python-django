#!/bin/python3

def sort_list(ll):
    new_list = []

    while ll:
        minimum = ll[0] 
        for x in ll: 
            if x < minimum:
                minimum = x
        new_list.append(minimum)
        ll.remove(minimum)    

    return(new_list)

def find_name(dictir, this_list):
    i = 0
    while i < len(this_list):
        # print(this_list[i])
        var = str(this_list[i])
        res = dictir.get('1942')
        print(res)
        i += 1
    

if __name__ == '__main__':
    d = {
        'Hendrix'   :   '1942',
        'Allman'    :   '1946',
        'King'      :   '1925',
        'Clapton'   :   '1945',
        'Johnson'   :   '1911',
        'Berry'     :   '1926',
        'Vaughan'   :   '1954',
        'Cooder'    :   '1947',
        'Page'      :   '1944',
        'Richards'  :   '1943',
        'Hammett'   :   '1962',
        'Cobain'    :   '1967',
        'Garcia'    :   '1942',
        'Beck'      :   '1944',
        'Santana'   :   '1947',
        'Ramone'    :   '1948',
        'White'     :   '1975',
        'Frusciante':   '1970',
        'Thompson'  :   '1949',
        'Burton'    :   '1939',
    }
    ll = []
    for x in d:
        ll.append(d[x])    
    ll = sort_list(ll)

    find_name(d, ll)
