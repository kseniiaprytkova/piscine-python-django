#!/bin/sh

if [ -n "$1" ]
then
	# The option “s” tells to curl to be silent, so will not print the “download” progress, “L” option tells that Curls must follow all redirections and “I” to download only the HTTP header, that is all we need. Lastly, we use grep to get header line that have redirection URL.
	# curl -sLI $1 | grep -i Location | cut -d ' ' -f 2
	curl -sI $1 | grep -i Location | cut -d ' ' -f 2
else
	echo "Error: No parameters found. "
fi
